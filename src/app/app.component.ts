import { Component } from '@angular/core';
import axios from 'axios';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    teams = [];                                                     // An object of all the loaded teams.
    teamLogos = {};                                                   // A map of teams, and their logos for easy access.
    gameToday = false;                                                // Boolean to say whether or not a game is on today.
    showNav = false;                                                    // Used to decide if to display nav or not.
    faveTeam = {id : null};                                             // Used to store the favorite team. Cant be null, otherwise system cant compare.
    zoneInfo = {                                                      // Used to contain the information for the zone.
        gameUpdater: null,                                              // Used for the timeout of the game updater.
        todaysGames: [],                                                // A list of all of todays games.
        open : true,                                                    // Used to say whether or not the zone is open.
        nextGame: null,                                                 // Used to hold the next game data.
        playedGames: {},                                                // Used to store what games have been played by the team.
        mode: 0,                                                        // Used to say what is displaying in the zone.
        loadedTeam: {id: null, name : 'Unknown', upcomming : [{ logo: null }]},   // Used to store the loaded teams information.
        loadedGame: {complete: null}                                    // Used to store the loaded games information.
    };

    /**
     * Used to find all of the games that will be running today.
     */
    loadTodaysGames() {
        this.zoneInfo.mode = 2;
        if (!this.zoneInfo.open) {
            this.zoneInfo.open = true;
        }
        this.zoneInfo.todaysGames = [];
        const today = new Date();
        axios.get('https://api.squiggle.com.au/?q=games').then(response => {
            response.data.games.forEach(game => {
                const gameDate = new Date(game.date);
                if (
                    gameDate.getDate() === today.getDate() &&
                    gameDate.getMonth() === today.getMonth() &&
                    gameDate.getFullYear() === today.getFullYear()
                ) {
                    game.ateamlogo = this.teamLogos[game.ateam];
                    game.hteamlogo = this.teamLogos[game.hteam];
                    game.state = (game.winner === null ? gameDate.getHours() > today.getHours() ? 'later' : 'live' : 'finished');
                    this.zoneInfo.todaysGames.push(game);
                }
            });
        });
    }

    /**
     * Used to load a team into the menu zone.
     * @param team
     */
    loadTeam(team) {
        if (team === this.zoneInfo.loadedTeam && this.zoneInfo.mode === 0) {
            this.zoneInfo.open = !this.zoneInfo.open;
        } else {
            this.zoneInfo.open = true;
            this.zoneInfo.mode = 0;
        }
        this.zoneInfo.loadedTeam = team;
    }

    /**
     * Used by the ui to format a date.
     * @param date
     */
    getDay(date) {
        const d = new Date(date);
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        return days[d.getDay()] + ' the ' + d.getDate() + this.nth(d.getDate());
    }

    /**
     * Used to load information from the squiggle api about a selected game id.
     * @param id
     */
    loadGame(id) {
        axios.get('https://api.squiggle.com.au/?q=games;game=' + id).then( response => {
            response.data.games[0].ateamlogo = this.teamLogos[response.data.games[0].ateam];
            response.data.games[0].hteamlogo = this.teamLogos[response.data.games[0].hteam];
            this.zoneInfo.loadedGame = response.data.games[0];
            this.zoneInfo.mode = 3;
        });
    }

    /**
     * Used for formatting the date
     * @param d, day
     */
    nth(d) {
        if (d > 3 && d < 21) { return 'th'; }
        switch (d % 10) {
            case 1:  return 'st';
            case 2:  return 'nd';
            case 3:  return 'rd';
            default: return 'th';
        }
    }

    /**
     * Takes game object from squiggle, and sorts the upcoming games
     * for both the teams involved in the game.
     * @param obj, Game Object from squiggle.
     */
    sortUpComingGames(obj) {
        if (obj.correct != null) {
            return;
        }
        const aTeam = obj.ateam;
        const hTeam = obj.hteam;
        let foundA = false;
        let foundH = false;

        const aTeamData = {
            opponent: aTeam, tip: obj.tip, tipteamid: obj.tipteamid, date: obj.date, confidence: obj.confidence,
            logo: this.teamLogos[aTeam]
        };
        const hTeamData = {
            opponent: hTeam, tip: obj.tip, tipteamid: obj.tipteamid, date: obj.date, confidence: obj.confidence,
            logo: this.teamLogos[hTeam]
        };
        // Adding the games to both teams.
        this.teams.forEach(team => {
            if (team.name === aTeam && !foundA) {
                if (!('upcomming' in team)) {
                    team.upcomming = [];
                }
                team.upcomming.push(hTeamData);
                foundA = true;
            }

            if (team.name === hTeam && !foundH) {
                if (!('upcomming' in team)) {
                    team.upcomming = [];
                }
                team.upcomming.push(aTeamData);
                foundH = true;
            }
        });
    }

    /**
     * Sort all played games, by adding each object passed into this
     * function to both of the teams involved.
     * @param obj, Game object from Squiggle.
     */
    sortPlayedGames(obj) {
        this.teams.forEach(team => {
            if (!('played' in team)) {
                team.played = [];
            }
            if (obj.ateam === team.name) {
                team.played.push({
                    winner: obj.winner,
                    ascore: obj.ascore,
                    hscore: obj.hscore,
                    opponent : obj.hteam,
                    logo: this.teamLogos[obj.hteam],
                    won : (obj.winner === obj.ateam ? 'Won' : 'Lost'),
                    id : obj.id
                });
            } else if (obj.hteam === team.name) {
                team.played.push({
                    winner: obj.winner,
                    ascore: obj.ascore,
                    hscore: obj.hscore,
                    opponent : obj.ateam,
                    logo: this.teamLogos[obj.ateam],
                    won : (obj.winner === obj.hteam ? 'Won' : 'Lost'),
                    id: obj.id
                });
            }
        });
    }

    /**
     * Sets the zoneInfo to open and changes the mode to '1'
     * so we know to display the nextGame.
     */
    getNextGame() {
        this.zoneInfo.open = true;
        this.zoneInfo.mode = 1;
    }

    /**
     * Used by the css, to determine whether or not to round the corners of the nav.
     */
    roundedNav() {
        return {roundedNav: this.zoneInfo.open};
    }

    /**
     * Used to set the favorite team for this browser. It will be used on login.
     * @param teamName
     */
    setFaveTeam(teamName) {
        localStorage.setItem('faveTeam', JSON.stringify(teamName));
        this.faveTeam = teamName;
    }

    faveTeamIcon() {
        return {selected : this.faveTeam.id === this.zoneInfo.loadedTeam.id};
    }

    showNavBar() {
        return { 'is-active' : this.showNav};
    }

    /**
     * Used to get the game that's being played today.
     * @param id
     */
    getTodaysGame(id) {
        this.loadGame(id);
        this.zoneInfo.gameUpdater = setTimeout(() => {
            axios.get('https://api.squiggle.com.au/?q=games;game=' + id).then( response => {
                response.data.games[0].ateamlogo = this.teamLogos[response.data.games[0].ateam];
                response.data.games[0].hteamlogo = this.teamLogos[response.data.games[0].hteam];
                this.zoneInfo.loadedGame = response.data.games[0];
            });
            if (this.zoneInfo.loadedGame.complete === 100) {
                clearTimeout(this.zoneInfo.gameUpdater);
            }
        }, 1000);
        // TODO: Sleep function on when to update the game once its started.

        // TODO: Sleep function to find when the game starts.
    }

    ngOnInit() {
        const promises = [];
        promises.push(axios.get('https://api.squiggle.com.au/?q=teams'));
        promises.push(axios.get('https://api.squiggle.com.au/?q=tips;source=11;complete=!100;year=2019'));
        promises.push(axios.get('https://api.squiggle.com.au/?q=games;year=2019;complete=100'));
        promises.push(axios.get('https://api.squiggle.com.au/?q=ladder'));
        // Doing this so they all run as a promise, then nothing can load without the other first.
        axios.all(promises).then(results => {
            // Getting the team information.
            const ranks = {};
            results[3].data.ladder.forEach(team => {
                ranks[team.rank] = team.teamid;
            });

            Object.keys(ranks).forEach(rank => {
                results[0].data.teams.forEach(team => {
                    if (ranks[rank] === team.id) {
                        this.teams.push(team);
                    }
                });
            });

            // Getting the team logos.
            this.teams.forEach(team => {
                this.teamLogos[team.name] = team.logo;
            });
            // Getting the upcoming games for each team.
            results[1].data.tips.forEach(game => {
                this.sortUpComingGames(game);
            });
            // Getting the played games for each team.
            results[2].data.games.forEach(game => {
                this.sortPlayedGames(game);
            });
            // Getting the nextGame.
            this.zoneInfo.nextGame = results[1].data.tips[0];
            // Preparing the next Game logos.
            this.zoneInfo.nextGame.ateamlogo = this.teamLogos[this.zoneInfo.nextGame.ateam];
            this.zoneInfo.nextGame.hteamlogo = this.teamLogos[this.zoneInfo.nextGame.hteam];

            // has started yet. If it has, load the live tab. Then you can see the live stats. (refresh every 1 minute).
            if (new Date(this.zoneInfo.nextGame.date).getDate() === new Date().getDate()) {
                this.gameToday = true;
            }

            // Loading the faveTeam
            this.faveTeam = (
                localStorage.getItem('faveTeam') !== null ?
                    JSON.parse(localStorage.getItem('faveTeam'))
                    : this.zoneInfo.loadedTeam
            );

            /**
             * Loading the users last known favorite team.
             */
            if (this.faveTeam !== null || this.faveTeam !== {id : null}) {
                this.loadTeam(this.faveTeam);
            }
        });
    }
}
